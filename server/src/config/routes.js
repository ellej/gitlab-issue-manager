export const ROUTE_PROJECTS_BASE = '/api/projects'
export const ROUTE_PROJECT_ISSUES = '/:id/issues'

export const ROUTE_WEBHOOKS_BASE = '/api/webhooks'
export const ROUTE_WEBHOOKS_ISSUES = '/issues'
