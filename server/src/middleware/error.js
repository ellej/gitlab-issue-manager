/**
 * Handle errors in the request processing pipeline.
 *
 * @param {object} err - Response error object
 * @param {object} req - Request object
 * @param {object} res - Response object
 * @param {Function} next - Middleware function to pass on the request
 * @returns {void} Nothing
 */
const handleError = (err, req, res, next) => {
  res
    .status(err.status || 500)
    .send({
      error: {
        message: err.message || 'Something went wrong...'
      }
    })
}

export default handleError
