/**
 * Check if the request is authorized.
 *
 * @param {object} req - Request object
 * @param {object} res - Response object
 * @param {Function} next - Middleware function to pass the request on
 * @returns {void} Nothing
 */
const checkAuthorized = (req, res, next) => {
  const token = process.env.GITLAB_SECRET_TOKEN

  if (!token || req.headers['x-gitlab-token'] !== token) {
    return next({
      status: 403,
      message: 'Access denied. Invalid token.'
    })
  }

  next()
}

export { checkAuthorized }
