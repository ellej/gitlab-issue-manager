/**
 * Make web socket server available in request processing pipeline.
 *
 * @param {object} io - The web socket server
 * @returns {Function} Function wrapped around the request
 */
const useSocket = (io) => {
  return (req, res, next) => {
    res.io = io

    next()
  }
}

export default useSocket
