import helmet from 'helmet'

/**
 * Prepare app for production by applying appropriate middleware.
 *
 * @param {object} app - The server application
 */
const prepareProd = (app) => {
  app.use(helmet())
  app.use(helmet.contentSecurityPolicy({
    directives: {
      ...helmet.contentSecurityPolicy.getDefaultDirectives(),
      'script-src': ["'self'"]
    }
  }))
}

export default prepareProd
