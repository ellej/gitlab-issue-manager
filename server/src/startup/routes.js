import cors from 'cors'
import express from 'express'

import useSocket from '../middleware/socket.js'
import projectsRouter from '../routes/projects.js'
import webhooksRouter from '../routes/webhooks.js'
import notFound404 from '../routes/404.js'
import errorHandler from '../middleware/error.js'
import { ROUTE_PROJECTS_BASE, ROUTE_WEBHOOKS_BASE } from '../config/routes.js'

/**
 * Register the routes for the application.
 *
 * @param {object} app - The server application
 * @param {object} io - The web socket server
 */
const registerRoutes = (app, io) => {
  app.use(cors())
  app.use(express.json())
  app.use(useSocket(io))

  app.use(ROUTE_PROJECTS_BASE, projectsRouter)
  app.use(ROUTE_WEBHOOKS_BASE, webhooksRouter)
  app.use(notFound404)
  app.use(errorHandler)
}

export default registerRoutes
