import manageSocket from '../socket/socketManager.js'

/**
 * Set up the web socket manager.
 *
 * @param {object} io - The web socket server
 * @returns {void} Nothing
 */
const setUpSocketManager = (io) => manageSocket(io)

export default setUpSocketManager
