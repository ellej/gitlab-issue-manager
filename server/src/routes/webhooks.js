import express from 'express'

import { checkAuthorized } from '../middleware/auth.js'
import { createIssueEvent } from '../controllers/webhooks.js'
import { ROUTE_WEBHOOKS_ISSUES } from '../config/routes.js'

const router = express.Router()

router.post(ROUTE_WEBHOOKS_ISSUES, checkAuthorized, createIssueEvent)

export default router
