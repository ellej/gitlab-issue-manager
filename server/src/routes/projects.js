import express from 'express'

import { getAll } from '../controllers/issues.js'
import { ROUTE_PROJECT_ISSUES } from '../config/routes.js'

const router = express.Router()

router.get(ROUTE_PROJECT_ISSUES, getAll)

export default router
