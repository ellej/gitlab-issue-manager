import dotenv from 'dotenv'
import express from 'express'
import http from 'http'
import { Server as SocketServer } from 'socket.io'

import log from './startup/log.js'
import registerRoutes from './startup/routes.js'
import setUpSocketManager from './startup/socket.js'
import prepareProd from './startup/prod.js'
import { CLIENT_URL_DEV } from './config/urls.js'

dotenv.config()

const app = express()
const server = http.createServer(app)
const io = new SocketServer(server, {
  cors: {
    origin: [
      CLIENT_URL_DEV,
      /* add a client production URL (including port) if needed */
    ]
  }
})

log(app)
registerRoutes(app, io)
setUpSocketManager(io)
prepareProd(app)

const port = process.env.PORT || 5000
server.listen(port, () => console.log(`Server running on port ${port}...`))
