import fetch from 'node-fetch'

/**
 * Close an issue.
 *
 * @param {object} obj - The data
 * @param {string} obj.projectId - The ID of the project
 * @param {string} obj.issueId - The ID of the issue
 * @returns {Promise<object>} Promise that resolves to an object with data or error property
 */
const closeIssue = ({ projectId, issueId }) => {
  return _changeIssueState({ projectId, issueId, state: 'close' })
}

/**
 * Reopen an issue.
 *
 * @param {object} obj - The data
 * @param {string} obj.projectId - The ID of the project
 * @param {string} obj.issueId - The ID of the issue
 * @returns {Promise<object>} Promise that resolves to an object with data or error property
 */
const reopenIssue = ({ projectId, issueId }) => {
  return _changeIssueState({ projectId, issueId, state: 'reopen' })
}

/**
 * Change the state of an issue.
 *
 * @param {object} obj - The data
 * @param {string} obj.projectId - The ID of the project
 * @param {string} obj.issueId - The ID of the issue
 * @param {string} obj.state - The new state to change to
 * @returns {object} Object with data or error property
 */
const _changeIssueState = async ({ projectId, issueId, state }) => {
  try {
    if (!projectId || !issueId || !state) {
      return {
        error: {
          status: 400,
          message: 'Project ID, Issue ID, and state are required.'
        }
      }
    }

    const endpoint = `https://gitlab.com/api/v4/projects/${projectId}/issues/${issueId}`
    const options = {
      method: 'PUT',
      headers: {
        'PRIVATE-TOKEN': process.env.GITLAB_ACCESS_TOKEN,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        state_event: state
      })
    }

    const res = await fetch(endpoint, options)
    const resParsed = await res.json()
    if (res.status < 200 || res.status >= 300) {
      return {
        error: {
          status: res.status,
          message: resParsed.error
        }
      }
    }

    return {
      data: {
        issue: resParsed
      }
    }
  } catch (err) {
    return { error: err }
  }
}

export { closeIssue, reopenIssue }
