import events from '../utils/events.js'
import { closeIssue, reopenIssue } from './issueHandler.js'

/**
 * Manage web socket events.
 *
 * @param {object} io - The web socket server
 * @returns {void} Nothing
 */
const manageSocket = (io) => {
  io.on('connection', (socket) => {
    socket.on(events.on.CLOSE_ISSUE, async (args) => {
      const { data, error } = await closeIssue(args)
      if (error) {
        console.error('Error: ', error)
        return io.to(socket.id).emit(events.emit.ERROR, { error })
      }

      io.emit(events.emit.ISSUE_CLOSED, { data })
    })

    socket.on(events.on.REOPEN_ISSUE, async (args) => {
      const { data, error } = await reopenIssue(args)
      if (error) {
        console.error('Error: ', error)
        return io.to(socket.id).emit(events.emit.ERROR, { error })
      }

      io.emit(events.emit.ISSUE_REOPENED, { data })
    })

    socket.on(events.on.USER_DISCONNECT, socket.disconnect)

    socket.on('disconnect', () => {
      console.log('Disconnected from socket.')
    })
  })
}

export default manageSocket
