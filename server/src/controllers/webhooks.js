import events from '../utils/events.js'

/**
 * Create an issue event.
 *
 * @param {object} req - Request object
 * @param {object} res - Response object
 * @param {Function} next - Middleware function to pass the request on
 */
const createIssueEvent = (req, res, next) => {
  const { object_attributes: issue } = req.body

  let event = ''
  switch (issue.action) {
    case 'open':
      event = events.emit.NEW_ISSUE
      break
    case 'close':
      event = events.emit.ISSUE_CLOSED
      break
    case 'reopen':
      event = events.emit.ISSUE_REOPENED
      break
    case 'update':
      event = events.emit.ISSUE_UPDATED
      break
    default:
      console.error(`Unsupported action: ${issue.action}`)
  }

  issue.labels = issue.labels.map(label => label.title)

  res.io.emit(event, { data: { issue } })

  // Always return a valid HTTP response to GitLab,
  // otherwise it thinks the hook failed and retries it.
  res.status(204).send()
}

export { createIssueEvent }
