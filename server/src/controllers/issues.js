import fetch from 'node-fetch'

/**
 * Get all the issues.
 *
 * @param {object} req - Request object
 * @param {object} res - Response object
 * @param {Function} next - Middleware function to pass the request on
 * @returns {void} Nothing
 */
const getAll = async (req, res, next) => {
  try {
    const projectId = req.params.id
    const endpoint = `https://gitlab.com/api/v4/projects/${projectId}/issues`
    const issuesRes = await fetch(endpoint, {
      method: 'GET',
      headers: {
        'PRIVATE-TOKEN': process.env.GITLAB_ACCESS_TOKEN,
        'Content-Type': 'application/json'
      }
    })

    const issuesResParsed = await issuesRes.json()
    if (issuesRes.status < 200 || issuesRes.status >= 300) {
      return next({
        status: issuesRes.status,
        message: issuesResParsed.message
      })
    }

    res.send({ data: { issues: issuesResParsed } })
  } catch (err) {
    next(err)
  }
}

export { getAll }
