import { useState, useEffect, useContext } from 'react'
import { io } from 'socket.io-client'

import {
  addOpenedIssue as handleOpenedIssue,
  closeIssue as handleClosedIssue,
  reopenIssue as handleReopenedIssue,
  updateIssue as handleUpdatedIssue
} from '../actions/issues'
import { IssuesDispatchContext } from '../contexts/issues'
import { LoadingSetContext } from '../contexts/loading'
import { ErrorSetContext } from '../contexts/errors'
import events from '../utils/events'

/**
 * Connect to and manage the web socket.
 *
 * @param {string} url - The URL to connect to the web socket
 * @returns {object} Object containing functions to emit events
 */
function useWebSocket (url) {
  const [socket] = useState(() => io(url))
  const issueDispatcher = useContext(IssuesDispatchContext)
  const addError = useContext(ErrorSetContext)
  const startLoader = useContext(LoadingSetContext)

  useEffect(() => {
    socket.on('connect', handleConnected)

    socket.on(events.on.NEW_ISSUE, ({ data }) => handleOpenedIssue(data.issue, issueDispatcher))

    socket.on(events.on.ISSUE_CLOSED, ({ data }) => handleClosedIssue(data.issue, issueDispatcher))

    socket.on(events.on.ISSUE_REOPENED, ({ data }) => handleReopenedIssue(data.issue, issueDispatcher))

    socket.on(events.on.ISSUE_UPDATED, ({ data }) => handleUpdatedIssue(data.issue, issueDispatcher))

    socket.on(events.on.ERROR, ({ error }) => addError(error))

    socket.on('disconnect', handleDisconnected)

    return () => sendDisconnect()
  }, [socket])

  /**
   * Handle being connected to the web socket.
   */
  const handleConnected = () => {
    console.log('Connected to web socket.')
  }

  /**
   * Handle being disconnected from the web socket.
   */
  const handleDisconnected = () => {
    console.log('Disconnected from web socket.')
  }

  /**
   * Close an issue.
   *
   * @param {object} data - The data to send to the server
   */
  const closeIssue = (data) => {
    startLoader()
    socket.emit(events.emit.CLOSE_ISSUE, data)
  }

  /**
   * Reopen an issue.
   *
   * @param {object} data - The data to send to the server
   */
  const reopenIssue = (data) => {
    startLoader()
    socket.emit(events.emit.REOPEN_ISSUE, data)
  }

  /**
   * Send a disconnecting message to the server.
   */
  const sendDisconnect = () => {
    socket.emit(events.emit.USER_DISCONNECT)
  }

  return { closeIssue, reopenIssue }
}

export default useWebSocket
