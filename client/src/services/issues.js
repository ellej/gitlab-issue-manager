import http from './http'

import { SERVER_ISSUES_URL } from '../config/endpoints'

/**
 * Make a GET request to get all the issues.
 *
 * @returns {Promise<object>} Promise that resolves to an object with data or error property
 */
const getAll = () => http.get(SERVER_ISSUES_URL)

const issuesApi = { getAll }

export default issuesApi
