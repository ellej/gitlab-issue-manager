/**
 * Make a GET request.
 *
 * @param {string} url - The URL to send request to
 * @param {object} options - Options for the HTTP request
 * @returns {Promise<object>} Promise that resolves to an object with data or error property
 */
const get = async (url, options = {}) => {
  try {
    const res = await fetch(url, { ...options, method: 'GET' })
    return res.json()
  }
  catch (err) {
    return { error: { message: err.message } }
  }
}

const http = { get }

export default http
