import { useContext } from 'react'
import { createUseStyles } from 'react-jss'

import Layout from '../components/Layout'
import IssuesList from '../components/IssuesList'
import useWebSocket from '../hooks/useWebSocket'
import { IssuesContext } from '../contexts/issues'
import { SERVER_BASE_URL } from '../config/endpoints'
import { PROJECT_ID } from '../config/project'
import { colors } from '../styles/common'

/**
 * Render the homepage.
 *
 * @param {object} props - Params
 * @returns {JSX.Element} A JSX element
 */
function Home (props) {
  const issues = useContext(IssuesContext)
  const { closeIssue, reopenIssue } = useWebSocket(SERVER_BASE_URL)

  const classes = useStyles()

  return (
    <Layout
      title='Issues'
      subtitle={`(Project ID: ${PROJECT_ID})`}
    >
      <div className={classes.lists}>
        <IssuesList
          issues={issues.opened}
          title='Opened'
          titleBgColor={colors.green}
          close={closeIssue}
        />
        <IssuesList
          issues={issues.closed}
          title='Closed'
          titleBgColor={colors.redLight}
          reopen={reopenIssue}
        />
      </div>
    </Layout>
  )
}

const useStyles = createUseStyles({
  lists: {
    width: '100%',
    maxWidth: '1200px',
    height: '100%',
    margin: '0 auto',
    display: 'flex',
    justifyContent: 'center'
  }
})

export default Home
