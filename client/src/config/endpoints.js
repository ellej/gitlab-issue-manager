import { PROJECT_ID } from './project.js'

const SERVER_BASE_URL_PROD = '<SERVER BASE URL FOR PRODUCTION IF NEEDED>'
const SERVER_BASE_URL_DEV = 'http://localhost:5000'

export const SERVER_BASE_URL = process.env.NODE_ENV === 'production'
  ? SERVER_BASE_URL_PROD
  : SERVER_BASE_URL_DEV

export const SERVER_ISSUES_URL = `${SERVER_BASE_URL}/api/projects/${PROJECT_ID}/issues`
