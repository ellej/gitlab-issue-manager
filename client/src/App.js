import { IssuesProvider } from './contexts/issues'
import { ErrorProvider } from './contexts/errors'
import { LoadingProvider } from './contexts/loading'
import Home from './views/Home'

/**
 * Render the application.
 *
 * @returns {JSX.Element} A JSX element
 */
function App () {
  return (
    <ErrorProvider>
      <LoadingProvider>
        <IssuesProvider>
          <Home />
        </IssuesProvider>
      </LoadingProvider>
    </ErrorProvider>
  )
}

export default App
