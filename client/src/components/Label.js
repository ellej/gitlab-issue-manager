import { createUseStyles } from 'react-jss'

import { colors, sizes, fonts } from '../styles/common'

/**
 * Render a label.
 *
 * @param {object} props - Params
 * @param {string} props.text - The label text
 * @returns {JSX.Element} A JSX element
 */
function Label ({ text, color = colors.blueLight }) {
  const classes = useStyles({ bgColor: color })

  return (
    <span className={classes.label}>
      {text}
    </span>
  )
}

const useStyles = createUseStyles({
  label: {
    margin: '5px',
    padding: '5px 10px',
    borderRadius: sizes.borderRadiusMedium,
    backgroundColor: ({ bgColor }) => bgColor,
    color: colors.white,
    fontSize: fonts.sizeSmall,
    fontWeight: 'bold',
    textTransform: 'lowercase'
  }
})

export default Label
