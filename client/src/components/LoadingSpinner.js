import { createUseStyles } from 'react-jss'
import Loader from 'react-loader-spinner'

import { colors } from '../styles/common'

/**
 * Render a loading spinner.
 *
 * @param {object} props - Params
 * @param {number} props.timeout - The duration of the spinner (in milliseconds)
 * @returns {JSX.Element} A JSX element
 */
function LoadingSpinner ({ timeout }) {
  const classes = useStyles()

  return (
    <div className={classes.loadingContainer}>
      <Loader
        type='ThreeDots'
        color={colors.purpleLight}
        height={200}
        width={200}
        timeout={timeout}
      />
    </div>
  )
}

const useStyles = createUseStyles({
  loadingContainer: {
    width: '100vw',
    height: '100vh',
    position: 'absolute',
    top: 0,
    left: 0,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(251, 251, 251, 0.6)',
    zIndex: 100
  }
})

export default LoadingSpinner
