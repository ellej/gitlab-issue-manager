import { useContext } from 'react'
import { createUseStyles } from 'react-jss'

import Alert from './Alert'
import Header from './Header'
import Footer from './Footer'
import LoadingSpinner from './LoadingSpinner'
import { LoadingContext } from '../contexts/loading'
import { ErrorContext } from '../contexts/errors'
import { colors, fonts, sizes } from '../styles/common'

/**
 * Render a page layout.
 *
 * @param {object} props - Params
 * @param {string} props.title - The title of the page using the layout
 * @param {string} props.subtitle - The subtitle of the page using the layout
 * @param {React.ReactNode} props.children - The child components
 * @returns {JSX.Element} A JSX element
 */
function Layout ({ title, subtitle, children }) {
  const error = useContext(ErrorContext)
  const loading = useContext(LoadingContext)

  const classes = useStyles()

  return (
    <div className={classes.container}>
      <Header />
      <main className={classes.innerContainer}>
        {error && (
          <Alert
            message={error.status < 500 ? error.message : 'Oops.. something went wrong.'}
            color={colors.redLight}
          />
        )}
        {loading && <LoadingSpinner timeout={3000} />}
        <h1>{title}</h1>
        <h2 className={classes.subtitle}>{subtitle}</h2>
        {children}
      </main>
      <Footer />
    </div>
  )
}

const useStyles = createUseStyles({
  container: {
    width: '100vw',
    minHeight: '100vh',
    backgroundColor: colors.almostWhite,
    color: colors.black,
    fontFamily: fonts.primary
  },
  innerContainer: {
    position: 'relative',
    width: '100vw',
    minHeight: `calc(100vh - ${sizes.headerHeight} - ${sizes.footerHeight})`,
    padding: '50px'
  },
  subtitle: {
    marginTop: '10px',
    marginBottom: '30px',
    fontSize: fonts.sizeMedium,
    color: colors.grayMedium
  }
})

export default Layout
