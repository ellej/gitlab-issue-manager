import { createUseStyles } from 'react-jss'

import { colors, sizes } from '../styles/common'

/**
 * Render the header.
 *
 * @param {object} props - Params
 * @returns {JSX.Element} A JSX element
 */
function Header (props) {
  const classes = useStyles()

  return (
    <header className={classes.header}>
      <h2>📝 GitLab Issue Manager</h2>
    </header>
  )
}

const useStyles = createUseStyles({
  header: {
    width: '100vw',
    height: sizes.headerHeight,
    padding: '0 50px',
    display: 'flex',
    alignItems: 'center',
    borderBottom: `1px solid ${colors.grayLight}`,
    boxShadow: '0 2px 8px 0 rgba(0, 0, 0, .10)',
    backgroundColor: colors.white
  }
})

export default Header
