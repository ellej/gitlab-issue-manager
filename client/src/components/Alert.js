import { createUseStyles } from 'react-jss'

import { colors, sizes } from '../styles/common'

/**
 * Render an alert.
 *
 * @param {object} props - Params
 * @param {string} props.message - The message to show
 * @param {string} props.color - The color of the alert box
 * @returns {JSX.Element} A JSX element
 */
function Alert ({ message, color = colors.blueLight }) {
  const classes = useStyles({ color })

  return (
    <div className={classes.error}>
      <p className={classes.message}>
        {message}
      </p>
    </div>
  )
}

const useStyles = createUseStyles({
  error: {
    position: 'absolute',
    top: '40px',
    left: '50%',
    transform: 'translateX(-50%)',
    padding: '10px 20px',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: sizes.borderRadiusMedium,
    backgroundColor: ({ color }) => color,
    transition: 'all 500ms ease-out'
  },
  message: {
    color: colors.white,
    fontWeight: 'bold'
  }
})

export default Alert
