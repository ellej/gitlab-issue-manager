import { createUseStyles } from 'react-jss'

import Issue from './Issue'
import { sizes } from '../styles/common'

/**
 * Render a list of issues.
 *
 * @param {object} props - Params
 * @param {Array} props.issues - The issues
 * @param {string} props.title - The title of the list
 * @param {string} props.titleBgColor - The background color of the title
 * @param {Function} props.close - A function to close an issue
 * @param {Function} props.reopen - A function to reopen an issue
 * @returns {JSX.Element} A JSX element
 */
function IssuesList ({ issues, title, titleColor = 'white', titleBgColor, close, reopen }) {
  const classes = useStyles({ titleColor, titleBgColor })

  return (
    <div className={classes.list}>
      <h2 className={classes.title}>{title}</h2>
      {issues.map(issue => (
        <Issue
          key={issue.id}
          issueId={issue.iid}
          title={issue.title}
          description={issue.description}
          labels={issue.labels}
          actionText={issue.state === 'opened' ? 'Close' : 'Reopen'}
          onActionClick={issue.state === 'opened'
            ? () => close({ projectId: issue.project_id, issueId: issue.iid })
            : () => reopen({ projectId: issue.project_id, issueId: issue.iid })
          }
        />
      ))}
    </div>
  )
}

const useStyles = createUseStyles({
  list: {
    margin: '0 50px',
    flex: 1
  },
  title: {
    width: 'max-content',
    marginBottom: '40px',
    padding: '8px 14px',
    borderRadius: sizes.borderRadiusSmall,
    boxShadow: '0px 3px 5px rgba(0, 0, 0, 0.20)',
    backgroundColor: ({ titleBgColor }) => titleBgColor,
    color: ({ titleColor }) => titleColor
  }
})

export default IssuesList
