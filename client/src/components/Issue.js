import { createUseStyles } from 'react-jss'

import ActionBtn from './ActionBtn'
import Label from './Label'
import { colors, sizes, fonts } from '../styles/common'

/**
 * Render a GitLab issue.
 *
 * @param {object} props - Params
 * @param {number} props.issueId - The issue ID
 * @param {string} props.title - The issue title
 * @param {string} props.description - The issue description
 * @param {Array} props.labels - The labels of the issue
 * @param {string} props.actionText - The text for the action button
 * @param {Function} props.onActionClick - A function to run when user clicks on action button
 * @returns {JSX.Element} A JSX element
 */
function Issue ({ issueId, title, description, labels, actionText, onActionClick }) {
  const classes = useStyles()

  return (
    <div className={classes.issue}>
      <div className={classes.header}>
        <p className={classes.id}>
          #{issueId}
        </p>
        <ActionBtn
          onClick={onActionClick}
          text={actionText}
        />
      </div>
      <div className={classes.contentContainer}>
        <div className={classes.block}>
          <h4>{title}</h4>
        </div>
        <div className={classes.block}>
          <p>
            {description}
          </p>
        </div>
        <div className={`${classes.block} ${classes.labels}`}>
          {labels.length ? labels.map(label => (
            <Label
              key={label}
              text={label}
            />
          )) : <Label text='no labels' />}
        </div>
      </div>
    </div>
  )
}

const useStyles = createUseStyles({
  issue: {
    width: '100%',
    marginBottom: '30px',
    transition: 'all 200ms ease-out',
    '&:hover': {
      transform: 'translateY(-5px)'
    }
  },
  header: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'flex-end'
  },
  contentContainer: {
    width: '100%',
    height: 'auto',
    border: `1px solid ${colors.grayLight}`,
    borderRadius: sizes.borderRadiusMedium,
    borderTopLeftRadius: 0,
    boxShadow: '0px 5px 8px rgba(0, 0, 0, 0.20)',
    backgroundColor: colors.white
  },
  id: {
    padding: '8px 10px',
    borderTopLeftRadius: sizes.borderRadiusSmall,
    borderTopRightRadius: sizes.borderRadiusSmall,
    backgroundColor: colors.grayLight,
    color: colors.grayDark,
    fontSize: fonts.sizeSmall,
    fontWeight: 'bold',
    letterSpacing: '2px'
  },
  block: {
    width: '100%',
    padding: '12px',
    display: 'flex',
    alignItems: 'center',
    borderBottom: `1px solid ${colors.grayLight}`
  },
  labels: {
    flexWrap: 'wrap',
    padding: '10px',
    borderBottom: 'none'
  }
})

export default Issue
