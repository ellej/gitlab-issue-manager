import { createUseStyles } from 'react-jss'

import { colors, sizes, fonts } from '../styles/common'

/**
 * Render an action button.
 *
 * @param {object} props - Params
 * @param {string} props.text - The button text
 * @param {Function} props.onClick - A function to run when user clicks on button
 * @returns {JSX.Element} A JSX element
 */
function ActionBtn ({ text, onClick }) {
  const classes = useStyles()

  return (
    <button
      className={classes.btn}
      onClick={onClick}
    >
      {text}
    </button>
  )
}

const useStyles = createUseStyles({
  btn: {
    display: 'block',
    marginBottom: '5px',
    padding: '5px 10px',
    border: 'none',
    borderRadius: sizes.borderRadiusMedium,
    boxShadow: `0 2px 10px -5px ${colors.black}`,
    background: colors.purpleLight,
    color: colors.white,
    fontSize: fonts.sizeSmall,
    fontWeight: 'bold',
    fontFamily: 'inherit',
    textTransform: 'lowercase',
    outline: 'none',
    cursor: 'pointer',
    transition: 'all 100ms ease-in-out',
    '&:hover': {
      boxShadow: `0 2px 14px -5px ${colors.black}`
    }
  }
})

export default ActionBtn
