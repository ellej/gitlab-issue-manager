import { createUseStyles } from 'react-jss'

import { colors, sizes } from '../styles/common'

/**
 * Render the footer.
 *
 * @param {object} props - Params
 * @returns {JSX.Element} A JSX element
 */
function Footer (props) {
  const classes = useStyles()

  return (
    <footer className={classes.footer}>
      <p>
        A React.js and Node.js web application.
      </p>
    </footer>
  )
}

const useStyles = createUseStyles({
  footer: {
    width: '100vw',
    height: sizes.footerHeight,
    padding: '0 70px',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderTop: `1px solid ${colors.grayLight}`,
    backgroundColor: colors.white,
    color: colors.grayMedium
  }
})

export default Footer
