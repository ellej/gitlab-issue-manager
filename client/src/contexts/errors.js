import { useState, useRef, useEffect, createContext } from 'react'

const ErrorContext = createContext()
const ErrorSetContext = createContext()

/**
 * Render an errors provider.
 *
 * @param {object} props - Params
 * @param {React.ReactNode} props.children - The children of the component
 * @returns {JSX.Element} A JSX element
 */
function ErrorProvider ({ children }) {
  const [error, setError] = useState(null)
  const timeoutIdRef = useRef(null);

  /**
   * Add an error.
   *
   * @param {object} err - The error to add
   */
  const addError = (err) => {
    setError(err)
    timeoutRemoveError(3000)
  }

  /**
   * Set a timeout for removing the error.
   *
   * @param {number} milliseconds - The duration of the timeout
   */
  const timeoutRemoveError = (milliseconds) => {
    // store the timeout id so it can be cleared in useEffect
    // when the component unmounts
    timeoutIdRef.current = setTimeout(() => setError(null), milliseconds)
  }

  useEffect(() => {
    return () => {
      if (timeoutIdRef.current)
        clearTimeout(timeoutRemoveError)
    }
  }, [])

  return (
    <ErrorContext.Provider value={error}>
      <ErrorSetContext.Provider value={addError}>
        {children}
      </ErrorSetContext.Provider>
    </ErrorContext.Provider>
  )
}

export { ErrorContext, ErrorSetContext, ErrorProvider }
