import { useEffect, useReducer, useContext, createContext } from 'react'

import issuesApi from '../services/issues'
import reducer from '../reducers/issues'
import { ErrorSetContext } from './errors'
import { initIssues } from '../actions/issues'

const IssuesContext = createContext()
const IssuesDispatchContext = createContext()

const defaultIssues = { opened: [], closed: [] }

/**
 * Render an issues provider.
 *
 * @param {object} props - Params
 * @param {React.ReactNode} props.children - The children of the component
 * @returns {JSX.Element} A JSX element
 */
function IssuesProvider ({ children }) {
  const [issues, dispatch] = useReducer(reducer, defaultIssues)
  const addError = useContext(ErrorSetContext)

  /**
   * Load the issues.
   *
   * @returns {void} Nothing
   */
  const loadIssues = async () => {
    const { error, data } = await issuesApi.getAll()
    if (error) {
      return addError(error)
    }

    initIssues(data.issues, dispatch)
  }

  useEffect(() => {
    loadIssues()
  }, [])

  return (
    <IssuesContext.Provider value={issues}>
      <IssuesDispatchContext.Provider value={dispatch}>
        {children}
      </IssuesDispatchContext.Provider>
    </IssuesContext.Provider>
  )
}

export { IssuesContext, IssuesDispatchContext, IssuesProvider }
