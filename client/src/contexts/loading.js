import { useState, useEffect, createContext } from 'react'

const LoadingContext = createContext()
const LoadingSetContext = createContext()

/**
 * Render a loadings provider.
 *
 * @param {object} props - Params
 * @param {React.ReactNode} props.children - The children of the component
 * @returns {JSX.Element} A JSX element
 */
function LoadingProvider ({ children }) {
  const [loading, setLoading] = useState(false)

  /**
   * Set loading to true.
   */
  const startLoader = () => {
    setLoading(true)
    timeoutStopLoader(2500)
  }

  /**
   * Set loading to false.
   *
   * @param {number} milliseconds - The duration of the timeout
   */
  const timeoutStopLoader = (milliseconds) => {
    setTimeout(() => setLoading(false), milliseconds)
  }

  useEffect(() => {
    return () => clearTimeout(timeoutStopLoader)
  }, [])

  return (
    <LoadingContext.Provider value={loading}>
      <LoadingSetContext.Provider value={startLoader}>
        {children}
      </LoadingSetContext.Provider>
    </LoadingContext.Provider>
  )
}

export { LoadingContext, LoadingSetContext, LoadingProvider }
