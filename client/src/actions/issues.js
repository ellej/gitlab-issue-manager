import types from './types'

/**
 * Initialize the issues.
 *
 * @param {Array} issues - All issues
 * @param {Function} dispatch - The dispatch function to use
 */
const initIssues = (issues, dispatch) => {
  dispatch({
    type: types.INIT_ISSUES,
    issues: {
      opened: _getOpenedIssues(issues),
      closed: _getClosedIssues(issues)
    }
  })
}

/**
 * Add an opened issue.
 *
 * @param {object} issue - The opened issue
 * @param {Function} dispatch - The dispatch function to use
 */
const addOpenedIssue = (issue, dispatch) => {
  dispatch({
    type: types.ADD_OPENED_ISSUE,
    issue
  })
}

/**
 * Close an issue.
 *
 * @param {object} issue - The issue
 * @param {Function} dispatch - The dispatch function to use
 */
const closeIssue = (issue, dispatch) => {
  dispatch({
    type: types.CLOSE_ISSUE,
    issue
  })
}

/**
 * Reopen an issue.
 *
 * @param {object} issue - The issue
 * @param {Function} dispatch - The dispatch function to use
 */
const reopenIssue = (issue, dispatch) => {
  dispatch({
    type: types.REOPEN_ISSUE,
    issue
  })
}

/**
 * Update an issue.
 *
 * @param {object} issue - The updated issue
 * @param {Function} dispatch - The dispatch function to use
 */
const updateIssue = (issue, dispatch) => {
  dispatch({
    type: types.UPDATE_ISSUE,
    issue
  })
}

/**
 * Get opened issues.
 *
 * @param {Array} issues - All issues
 * @returns {Array} The opened issues
 */
const _getOpenedIssues = (issues) => issues.filter(issue => issue.state === 'opened')

/**
 * Get closed issues.
 *
 * @param {Array} issues - All issues
 * @returns {Array} The closed issues
 */
const _getClosedIssues = (issues) => issues.filter(issue => issue.state === 'closed')

export {
  initIssues,
  addOpenedIssue,
  closeIssue,
  reopenIssue,
  updateIssue
}
