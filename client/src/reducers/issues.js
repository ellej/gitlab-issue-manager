import types from '../actions/types'

/**
 * Reduce the current issues state to new states.
 *
 * @param {object} state - The current state (issues)
 * @param {object} action - The action object
 * @returns {object} The new updated state
 */
const reducer = (state, action) => {
  switch (action.type) {
    case types.INIT_ISSUES:
      return {
        opened: action.issues.opened,
        closed: action.issues.closed
      }
    case types.ADD_OPENED_ISSUE:
      return {
        opened: _exists(action.issue, state.opened)
          ? state.opened
          : [...state.opened, action.issue],
        closed: state.closed
      }
    case types.CLOSE_ISSUE:
      return {
        opened: state.opened.filter(issue => issue.id !== action.issue.id),
        closed: _exists(action.issue, state.closed)
          ? state.closed
          : [...state.closed, action.issue]
      }
    case types.REOPEN_ISSUE:
      return {
        opened: _exists(action.issue, state.opened)
          ? state.opened
          : [...state.opened, action.issue],
        closed: state.closed.filter(issue => issue.id !== action.issue.id)
      }
    case types.UPDATE_ISSUE:
      return {
        opened: state.opened.map(issue => issue.id === action.issue.id ? action.issue : issue),
        closed: state.closed.map(issue => issue.id === action.issue.id ? action.issue : issue)
      }
    default:
      throw new Error(`Unsupported action: ${action.type}`)
  }
}

/**
 * Checks if an item exists in a list.
 *
 * @param {object} item - The item
 * @param {number} item.id - The id of the item
 * @param {Array} list - The list to search in
 * @returns {boolean} Whether or not the item exists in the list
 */
const _exists = ({ id }, list) => {
  return list.some(item => item.id === id)
}

export default reducer
