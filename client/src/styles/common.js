const colors = {
  white: '#ffffff',
  almostWhite: '#fbfbfb',
  black: '#1e1e1e',
  grayLight: 'rgb(223, 223, 223)',
  grayMedium: 'rgb(187, 187, 187)',
  grayDark: 'rgb(147, 147, 147)',
  redLight: 'rgb(238, 110, 110)',
  green: 'rgb(155, 221, 155)',
  purpleLight: 'rgb(112, 112, 209)',
  blueLight: 'rgb(183, 205, 249)',
  yellowLight: 'rgb(238, 213, 154)'
}

const sizes = {
  headerHeight: '60px',
  footerHeight: '50px',
  borderRadiusSmall: '4px',
  borderRadiusMedium: '10px'
}

const fonts = {
  primary: 'Poppins, sans-serif',
  sizeSmall: '12px',
  sizeMedium: '20px'
}

export { colors, sizes, fonts }
