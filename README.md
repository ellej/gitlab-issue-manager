# Real-Time GitLab Issue Manager

A React.js and Node.js web application for tracking and managing your GitLab issues in real-time using web sockets and GitLab's webhooks REST API. The user can close or reopen a project's issues from the client application.

## Table of Contents

- [Tech Stack](#tech-stack)
- [Screenshot](#screenshot)
- [Data Flow](#data-flow)
- [Getting Started](#getting-started)
  - [Prerequisites](#prerequisites)
  - [Clone the repo](#clone-the-repo)
  - [Install dependencies](#install-dependencies)
  - [Create a GitLab access token](#create-a-gitlab-access-token)
  - [Configure the server](#configure-the-server)
  - [Configure the client](#configure-the-client)
  - [Expose localhost to the Internet](#expose-localhost-to-the-internet)
  - [Add a webhook in GitLab](#add-a-webhook-in-gitlab)
  - [Run the application](#run-the-application)
- [Useful Resources](#useful-resources)

## Tech Stack

* Node.js
* Express
* Socket.io
* React.js

## Screenshot

![GitLab issue manager screenshot](https://res.cloudinary.com/ellej/image/upload/c_scale,f_auto,q_auto,w_1920/v1632404672/screenshots/app-gitlab-issue-manager_zmxw7y.png)

## Data Flow

The diagram below illustrates how the data flows in the following scenario:

1. The user starts the client application.
2. The user sees all opened and closed issues of a particular project.
3. The user creates a new issue in GitLab and sees it appear on the client in real-time.
4. The user closes an issue from the client.
5. The user sees that the issue has been closed.

![Data flow of GitLab issue manager](https://res.cloudinary.com/ellej/image/upload/c_scale,f_auto,q_auto,w_710/v1632479520/ellej.dev/gitlab-issue-manager-data-flow_duancd.png)

## Getting Started

The following instructions explain how to run the application in **dev mode** locally.

### Prerequisites

This application requires that you have:

* [Node.js](https://nodejs.org/) LTS version 14+ installed
* A [GitLab](https://gitlab.com/) account with at least 1 project

### Clone the repo

Open a terminal window and change the directory to where you want to place the cloned repo directory, then run:

```bash
# using ssh
git clone git@gitlab.com:ellej/gitlab-issue-manager.git

# using https
git clone https://gitlab.com/ellej/gitlab-issue-manager.git
```

> The client and server applications are in **separate** folders in the cloned repo and are to be treated separately.

### Install dependencies

Use two **separate** terminal windows for installing client and server dependencies.

##### Server

```bash
cd gitlab-issue-manager/server
npm install
```

##### Client

```bash
cd gitlab-issue-manager/client
npm install
```

### Create a GitLab access token

This application uses webhooks (user-defined HTTP callbacks) in order for your server to be able to receive notifications from GitLab whenever a project's issue is created, updated, closed, or reopened.

In order for your server to authenticate with the GitLab API, you need to create a [personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html). That token will be scoped to you as a *user*. (If you are on a paid plan or a self-managed instance, you could instead create a [project access token](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html) to be scoped only to the specific *project* that you choose.)

1. In GitLab, select your avatar (usually in the top-right corner)
2. Go to `Edit profile`.
3. From the sidebar, go to `Access Tokens`
4. Enter a friendly name of your project as the token name (e.g. *GitLab Issue Manager*)
5. Select an expiry date for the token.
6. Select `api` as the scope.
7. Click `Create personal access token`.
8. Save the token somewhere safe.
    > ⚠️ Once you leave the page, you don't have access to the token again.

### Configure the server

In the root of the **server** directory of the project, create a file called `.env`.

This file will contain key-value pairs in the form of `<KEY>=<VALUE>` for each environment variable. You can view a sample of this in the file `sample.env`.

The following first three steps explain how to set the environment variables `PORT`, `GITLAB_ACCESS_TOKEN`, and `GITLAB_SECRET_TOKEN`. The last step sets a `CLIENT_URL_DEV` constant.

##### Add the port to listen on

Decide which port (e.g. `5000`) you want the server to listen for incoming requests from.

In `.env`, set the port number as the value to the `PORT` key.

```shell
PORT=<YOUR SERVER PORT NUMBER>
```

If you are developing locally and your port number is `5000`, the server will be listening at `http://localhost:5000`, which is the URL that will also be used when configuring the client.

##### Add the GitLab access token

Copy the access token you stored after you created it in the [previous section](#create-a-gitlab-access-token). Paste it as the value to the `GITLAB_ACCESS_TOKEN` key in the `.env` file.

```shell
GITLAB_ACCESS_TOKEN=<YOUR GITLAB API ACCESS TOKEN>
```

##### Add the GitLab secret token

In a few steps you will configure the webhook on GitLab and provide a secret token. This token will be used by your server to verify the incoming request.

Create a random string and set it as the value to the `GITLAB_SECRET_TOKEN` key in `.env`.

```shell
GITLAB_SECRET_TOKEN=<YOUR GITLAB WEBHOOK SECRET TOKEN>
```

##### Environment variables summary

You should now have three environment variables in `.env`.

```shell
PORT=<YOUR SERVER PORT NUMBER>
GITLAB_ACCESS_TOKEN=<YOUR GITLAB API ACCESS TOKEN>
GITLAB_SECRET_TOKEN=<YOUR GITLAB WEBHOOK SECRET TOKEN>
```

> **No** single or double quotes are needed for the environment variable values.

##### Add the dev client URL

The server will be listening for web socket events and some HTTP requests from the client, so it needs to know the client URL in order to permit those requests.

If you do not make any modifications to the port or URL that the React.js client application will run on, then the client URL for development will be `http://localhost:3000` and you do not need to configure anything.

To modify the dev client URL, from the **server** directory, open `/src/config/urls.js` and set the URL as the exported `CLIENT_URL_DEV` constant.

Example:

```javascript
export const CLIENT_URL_DEV = 'http://localhost:3000'
```

### Configure the client

##### Add the dev server URL

If you have set up your backend server as explained in the [previous section](#configure-the-server) you can copy the port number (e.g. `5000`) that the server is listening on.

Once copied, in the **client** directory, open `/src/config/endpoints.js` and paste it as part of the `SERVER_BASE_URL_DEV` constant.

Example:

```javascript
const SERVER_BASE_URL_DEV = 'http://localhost:5000'
```

##### Add your project ID

In GitLab, navigate to the project that you want to associate this application with.

Copy the `Project ID` next to the project title. (Alternatively, copy it from your project's `Settings > General` page.)

Once copied, in the **client** directory, open `/src/config/project.js` and paste it as the value of the `PROJECT_ID` constant.

Example:

```javascript
export const PROJECT_ID = '123456789'
```

### Expose localhost to the Internet

Since GitLab needs to be able to make HTTP requests to your server to notify you of updates, it needs to be accessible on the Internet.

For that, you can use the **free** and commonly used program [ngrok](https://ngrok.com/download) to very easily get a temporary public URL that will forward the requests to a port on your local development environment.

1. Log in or sign up to [ngrok](https://ngrok.com/).
2. Visit the dashboard and copy your auth token.
3. Go to the [download and setup ngrok page](https://ngrok.com/download).
4. Download, unzip, and run ngrok on your machine.
5. From the command line, navigate to the directory containing the ngrok program.
6. Connect your ngrok account by using the auth token you copied and running the following:
    ```bash
    ./ngrok authtoken <YOUR_AUTH_TOKEN>
    ```
7. Expose localhost on the same port you configured the server (e.g. `5000`) in the [previous section](#add-the-port-to-listen-on) by running the following:
    ```bash
    ./ngrok http 5000
    ```
8. You now may or may not see output in the form of an http and https forwarding address, e.g.:
    ```shell
    Forwarding         http://92832de0.ngrok.io -> localhost:5000
    Forwarding         https://92832de0.ngrok.io -> localhost:5000
    ```
    If you do not see an output, open a browser and navigate to `http://localhost:4040` where you can see both the http and https addresses. (You can also inspect all incoming traffic via that UI.)
9. Copy the **https** address and keep ngrok running.

> Note that the forwarding URL provided by ngrok will be different every time you restart it (on the free plan). So whenever you restart it, you will need to update that URL in the webhook settings in GitLab (see section below).

### Add a webhook in GitLab

You will now configure which server endpoint GitLab should send the requests to and on what specific events.

1. On your project page on GitLab, go to `Settings > Webhooks`.
2. Copy the ngrok URL from the [previous section](#expose-localhost-to-the-internet) and set as the webhook URL. Additionally, to the end of that URL add `/api/webhooks/issues` which is the exact route that the server expects GitLab's requests to go to. Example:
    ```shell
    https://92832de0.ngrok.io/api/webhooks/issues
    ```
3. Copy the `GITLAB_SECRET_TOKEN` from a [previous section](#add-the-gitlab-secret-token) and set as the secret token.
4. Select `Issues events` as the only trigger.
5. Enable SSL verification.
6. Click `Add webhook`.

Even though there is a `Test` button now next to the created webhook, we first need to start the server in order for it to work (see below). Additionally, in order to test, you need to **create at least 1 issue** in your GitLab project.

### Run the application

If you are developing locally and you want to receive notifications from GitLab whenever they detect changes to your issues, make sure you have exposed localhost to the Internet by having ngrok running as described in a [previous section](#expose-localhost-to-the-internet).

Use two **separate** terminal windows for running the client and server applications.

1. Start the server

```bash
cd gitlab-issue-manager/server
npm start
```

2. Start the client

```bash
cd gitlab-issue-manager/client
npm start
```

If your default browser does not open automatically, open a browser and navigate to `http://localhost:3000`.

🥳 You can now track your project's GitLab issues and close or reopen them via the client!

> Remember to stop ngrok when you are done testing your webhook, simply using `Ctrl+C` (on Windows) or `Cmd+C` (on Mac).

## Useful Resources

* [GitLab Webhooks](https://docs.gitlab.com/ee/user/project/integrations/webhooks.html#issue-events)
* [GitLab Issues API](https://docs.gitlab.com/ee/api/issues.html#edit-issue)
* [GitLab REST API Resources](https://docs.gitlab.com/ee/api/api_resources.html)
